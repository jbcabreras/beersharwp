﻿using BeerShare.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.System;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace BeerShare
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Login : Page
    {
        public Login()
        {
            this.InitializeComponent();
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;

            //criar usuario pra teste
            Usuario teste = new Usuario("a", "", "a", "a");
            Cadastro.cadastrar(teste);

        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Frame.Tag = "Login";
        }

        private void login(object sender, RoutedEventArgs e)
        {
            if (campoUsuario.Text.ToString() == "" || campoSenha.Password.ToString() == "")
            {
                camposDialogo();
            }
            else
            {
                Usuario usuario = Cadastro.confirmaEmailSenha(campoUsuario.Text.ToString(), campoSenha.Password.ToString());

                if (usuario != null)
                {
                    Sessao.setSessao(usuario);
                    Frame.Navigate(typeof(MainPage));
                }
                else
                {
                    erroDialogo();
                }
            }
        }

        private void cadastrar(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Cadastrar));
        }

        void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;

            Frame rootFrame = Window.Current.Content as Frame;
            if (rootFrame != null && rootFrame.CanGoBack && Frame.Tag.Equals("Login"))
            {
                e.Handled = true;
                Frame.GoBack();
            }

        }

        private async void camposDialogo()
        {
            MessageDialog dialog = new MessageDialog("Favor preencher os campos!", "Alerta");
            dialog.Commands.Add(new UICommand("OK"));
            await dialog.ShowAsync();
        }

        private async void erroDialogo()
        {
            MessageDialog dialog = new MessageDialog("Usuário ou senha inválidos!", "Alerta");
            dialog.Commands.Add(new UICommand("OK"));
            await dialog.ShowAsync();
        }

        private async void recuperar(object sender, RoutedEventArgs e)
        {

        }

        private void escondeTeclado(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                Windows.UI.ViewManagement.InputPane.GetForCurrentView().TryHide(); 
            }
        }

        private void senhaFocus(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                campoSenha.Focus(FocusState.Programmatic);
            }
        }
    }
}
