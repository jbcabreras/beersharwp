﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;
using BeerShare.Model;
using Windows.ApplicationModel.Core;
using Windows.Storage.Pickers;
using Windows.ApplicationModel.Activation;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml.Media.Imaging;
using System.Collections.ObjectModel;
using Windows.System;
using System.Threading.Tasks;
using Windows.UI.Core;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace BeerShare
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        CoreApplicationView view;
        private string ImagePath;
        private BitmapImage imagem = null;
        private Cerveja cevaItem = null;
        private List<Cerveja> listaUsuario = null;
        private List<Cerveja> listaTodasCevas = null;
        private List<Cerveja> listaBuscar = new List<Cerveja>();
        private ObservableCollection<Cerveja> listaMinha = new ObservableCollection<Cerveja>();
        private ObservableCollection<Cerveja> listaTodas = new ObservableCollection<Cerveja>();
        private ObservableCollection<Cerveja> listaResult = new ObservableCollection<Cerveja>();
        IAsyncOperation<IUICommand> asyncCommand = null;

        public MainPage()
        {
            this.InitializeComponent();

            this.DataContext = this;

            this.NavigationCacheMode = NavigationCacheMode.Required;

            HardwareButtons.BackPressed += HardwareButtons_BackPressed;

            view = CoreApplication.GetCurrentView();

            populaListas();

            MostraAvisoTodas();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.

            populaListas();

            Frame.Tag = "Main";

            MostraAvisoTodas();
        }

        private void MostraAvisoTodas()
        {
            if (listaTodas.Count() < 1)
            {
                avisoTodas.Visibility = Visibility.Visible;
                campoBuscar.Visibility = Visibility.Collapsed;
            }
        }

        void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;

            Frame rootFrame = Window.Current.Content as Frame;
            if (rootFrame != null && rootFrame.CanGoBack && Frame.Tag.Equals("Main"))
            {
                e.Handled = true;
                Frame.Navigate(typeof(Login));
                //Frame.GoBack();
            }

        }

        private async void adicionar(object sender, RoutedEventArgs e)
        {
            string combo = null;
            string nome = campoNome.Text;
            string caract = campoCaract.Text;

            if (fotoAdicionar.Tag.Equals("true") && nome != "" )
            {
                combo = ((ComboBoxItem)comboEstilo.SelectedItem).Content.ToString();

                Cerveja ceva = new Cerveja(imagem, combo, campoNome.Text, campoCaract.Text);
                ListaCervejas.adicionar(ceva);

                populaListas();

                limpaAdicionar();

                Menu.SelectedItem = menuMinha;
            }
            else
            {
                MessageDialog dialog = new MessageDialog("Favor preencher os campos!", "Alerta");
                dialog.Commands.Add(new UICommand("OK"));
                await ShowDialog(dialog);
            }

            Debug.WriteLine("combo: " + combo);
            Debug.WriteLine("nome: " + nome);
            Debug.WriteLine("caract: " + caract);
        }

        private void limpaAdicionar()
        {
            fotoAdicionar.Source = new BitmapImage(new Uri("ms-appx:///Assets/img/photos.png"));
            comboEstilo.SelectedIndex = 0;
            campoNome.Text = "";
            campoCaract.Text = "";
        }

        private void galeria(object sender, RoutedEventArgs e)
        {
            ImagePath = string.Empty;
            FileOpenPicker filePicker = new FileOpenPicker();
            filePicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            filePicker.ViewMode = PickerViewMode.Thumbnail;

            // Filter to include a sample subset of file types
            filePicker.FileTypeFilter.Clear();
            filePicker.FileTypeFilter.Add(".bmp");
            filePicker.FileTypeFilter.Add(".png");
            filePicker.FileTypeFilter.Add(".jpeg");
            filePicker.FileTypeFilter.Add(".jpg");

            filePicker.PickSingleFileAndContinue();
            view.Activated += viewActivated;

            imagem = new BitmapImage();
        }

        private async void viewActivated(CoreApplicationView sender, IActivatedEventArgs args1)
        {
            FileOpenPickerContinuationEventArgs args = args1 as FileOpenPickerContinuationEventArgs;

            if (args != null)
            {
                if (args.Files.Count == 0) return;

                view.Activated -= viewActivated;
                StorageFile storageFile = args.Files[0];
                var stream = await storageFile.OpenAsync(Windows.Storage.FileAccessMode.Read);
                var bitmapImage = new Windows.UI.Xaml.Media.Imaging.BitmapImage();
                await bitmapImage.SetSourceAsync(stream);

                var decoder = await Windows.Graphics.Imaging.BitmapDecoder.CreateAsync(stream);

                imagem = bitmapImage;
                fotoAdicionar.Source = bitmapImage;
                fotoAdicionar.Tag = "true";
            }
        }

        private void populaListas()
        {
            listaUsuario = ListaCervejas.getCervejasUsuario();
            listaMinha.Clear();

            if (listaUsuario.Count() > 0)
            {
                avisoMinhaLista.Visibility = Visibility.Collapsed;

                foreach (Cerveja ceva in listaUsuario)
                {
                    listaMinha.Add(ceva);
                }

                lvMinha.DataContext = listaMinha;
            }

            listaTodasCevas = ListaCervejas.getTodasCervejas();
            listaTodas.Clear();

            if (listaTodasCevas.Count() > 0)
            {
                avisoTodas.Visibility = Visibility.Collapsed;
                campoBuscar.Visibility = Visibility.Visible;

                foreach (Cerveja ceva in listaTodasCevas)
                {
                    listaTodas.Add(ceva);
                }

                lvTodas.DataContext = listaTodas;
            }
        }

        private void detalhesMinha(object sender, TappedRoutedEventArgs e)
        {
            Cerveja cevaItem = (Cerveja)lvMinha.SelectedItem;

            if (cevaItem != null)
            {
                Frame.Navigate(typeof(Detalhes), cevaItem); 
            }
        }

        private void detalhesTodas(object sender, TappedRoutedEventArgs e)
        {
            Cerveja cevaItem = (Cerveja)lvTodas.SelectedItem;

            if (cevaItem != null)
            {
                Frame.Navigate(typeof(Detalhes), cevaItem); 
            }
        }

        private void nomeFocus(object sender, object e)
        {
            campoNome.Focus(FocusState.Programmatic);
        }

        private void caractFocus(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                campoCaract.Focus(FocusState.Programmatic);
            }
        }

        private void escondeTeclado(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                Windows.UI.ViewManagement.InputPane.GetForCurrentView().TryHide();
            }
        }

        private void buscarCerveja(object sender, TextChangedEventArgs e)
        {
            listaBuscar = ListaCervejas.buscarCervejas(campoBuscar.Text);
            listaResult.Clear();

            if (listaBuscar.Count() > 0)
            {
                avisoTodas.Visibility = Visibility.Collapsed;

                foreach (Cerveja ceva in listaBuscar)
                {
                    listaResult.Add(ceva);
                }

                lvTodas.DataContext = listaResult;
            }
            else
            {
                listaTodas.Clear();
                lvTodas.DataContext = listaTodas;
            }
        }

        private async void excluir(object sender, HoldingRoutedEventArgs e)
        {
            FrameworkElement element = (FrameworkElement)e.OriginalSource;
            if (element.DataContext != null && element.DataContext is Cerveja)
            {
                cevaItem = (Cerveja)element.DataContext;

                if (cevaItem != null)
                {
                    MessageDialog mdExcluir = new MessageDialog("Deseja excluir esse ítem?");
                    mdExcluir.Commands.Add(new UICommand("Sim", new UICommandInvokedHandler(this.ComandoSim)));
                    mdExcluir.Commands.Add(new UICommand("Não", new UICommandInvokedHandler(this.ComandoNao)));

                    await ShowDialog(mdExcluir);
                }
            }

            if (listaMinha.Count() < 1)
            {
                avisoMinhaLista.Visibility = Visibility.Visible;
            }

            MostraAvisoTodas();
        }

        private void ComandoSim(IUICommand command)
        {
            ListaCervejas.excluirCerveja(cevaItem);
            populaListas();
        }
        private void ComandoNao(IUICommand command)
        {
            // Desenvolva uma Ação
        }

        private Task ShowDialog(MessageDialog dialog)
        {
            CoreDispatcher dispatcher = Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher;
            Func<object, Task<bool>> action = null;
            action = async (o) =>
            {
                try
                {
                    if (dispatcher.HasThreadAccess)
                        await dialog.ShowAsync();
                    else
                    {
                        await dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                        () => action(o));
                    }
                    return true;
                }
                catch (UnauthorizedAccessException)
                {
                    /*if (action != null)
                    {
                        Task.Delay(500).ContinueWith(async t => await action(o));
                    }*/
                }
                return false;
            };
            return action(null);
        }
    }
}
