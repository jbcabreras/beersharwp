﻿using BeerShare.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.System;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace BeerShare
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Cadastrar : Page
    {
        public Cadastrar()
        {
            this.InitializeComponent();
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Frame.Tag = "Cadastrar";
        }

        private void cadastrar(object sender, RoutedEventArgs e)
        {
            string nome = campoUsuario.Text.ToString();
            string telefone = campoTel.Text.ToString();
            string email = campoEmail.Text.ToString();
            string senha = campoSenha.Password.ToString();

            if (nome == "" || email == "" || senha == "")
            {
                JanelaDialogo();
            }
            else
            {
                Usuario usuario = new Usuario(nome, telefone, email, senha);

                Cadastro.cadastrar(usuario);

                Sessao.setSessao(usuario);
                
                Frame.Navigate(typeof(MainPage));
            }
        }

        void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;

            Frame rootFrame = Window.Current.Content as Frame;
            if (rootFrame != null && rootFrame.CanGoBack && Frame.Tag.Equals("Cadastrar"))
            {
                e.Handled = true;
                Frame.GoBack();
            }

        }

        private async void JanelaDialogo()
        {
            MessageDialog dialog = new MessageDialog("Favor preencher os campos!", "Alerta");
            dialog.Commands.Add(new UICommand("OK"));
            await dialog.ShowAsync();
        }

        private void whatsFocus(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                campoTel.Focus(FocusState.Programmatic);
            }
        }

        private void emailFocus(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                campoEmail.Focus(FocusState.Programmatic);
            }
        }

        private void senhaFocus(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                campoSenha.Focus(FocusState.Programmatic);
            }
        }

        private void escondeTeclado(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                Windows.UI.ViewManagement.InputPane.GetForCurrentView().TryHide();
            }
        }
    }
}
