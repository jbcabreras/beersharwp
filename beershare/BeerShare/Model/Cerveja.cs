﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;

namespace BeerShare.Model
{
    public class Cerveja
    {
        public BitmapImage Foto { get; set; }

        public string Tipo { get; set; }

        public string Nome { get; set; }

        public string Caracteristica { get; set; }

        public Cerveja(BitmapImage foto, string tipo, string nome, string caracteristica)
        {
            this.Foto = foto;
            this.Tipo = tipo;
            this.Nome = nome;
            this.Caracteristica = caracteristica;
        }

        public Cerveja() { }
    }
}
