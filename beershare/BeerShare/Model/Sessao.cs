﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerShare.Model
{
    public static class Sessao
    {
        private static Usuario sessao;

        static Sessao()
        {
            sessao = new Usuario();
        }

        public static void setSessao(Usuario usuario)
        {
            sessao = usuario;
        }

        public static Usuario getSessao()
        {
            return sessao;
        }

        public static string getUsuarioSessao()
        {
            return sessao.Nome;
        }
    }
}
