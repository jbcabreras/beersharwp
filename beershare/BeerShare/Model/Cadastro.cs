﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerShare.Model
{
    public static class Cadastro
    {
        private static List<Usuario> listaUsuarios;

        static Cadastro()
        {
            listaUsuarios = new List<Usuario>();
        }

        public static void cadastrar(Usuario usuario)
        {
            listaUsuarios.Add(usuario);
        }

        public static Usuario confirmaEmailSenha(string email, string senha)
        {
            foreach(Usuario usuario in listaUsuarios)
            {
                if(usuario.Email == email)
                {
                    if(usuario.Senha == senha)
                    {
                        return usuario;
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            return null;
        }
    }
}
