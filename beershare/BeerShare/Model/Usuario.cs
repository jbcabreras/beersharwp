﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerShare.Model
{
    public class Usuario
    {
        public string Nome { get; set; }

        public string Telefone { get; set; }

        public string Email { get; set; }

        public string Senha { get; set; }

        public Usuario() { }

        public Usuario(string nome, string tel, string email, string senha)
        {
            this.Nome = nome;
            this.Telefone = tel;
            this.Email = email;
            this.Senha = senha;
        }
    }
}
