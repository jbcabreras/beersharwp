﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerShare.Model
{
    public static class ListaCervejas
    {
        private static List<KeyValuePair<Usuario, Cerveja>> listaCervejas;

        static ListaCervejas()
        {
            listaCervejas = new List<KeyValuePair<Usuario, Cerveja>>();
        }

        public static void adicionar(Cerveja cerveja)
        {
            var chaveValor = new KeyValuePair<Usuario, Cerveja>(Sessao.getSessao(), cerveja);
            listaCervejas.Add(chaveValor);
        }

        public static List<Cerveja> getCervejasUsuario()
        {
            List<Cerveja> listaUsuario = new List<Cerveja>();

            foreach (KeyValuePair<Usuario, Cerveja> entry in listaCervejas)
            {
                if (entry.Key == Sessao.getSessao())
                {
                    listaUsuario.Add(entry.Value);
                }
            }

            return listaUsuario;
        }

        public static List<Cerveja> getTodasCervejas()
        {
            List<Cerveja> listaTodas = new List<Cerveja>();

            foreach (KeyValuePair<Usuario, Cerveja> entry in listaCervejas)
            {
                listaTodas.Add(entry.Value);
            }

            return listaTodas;
        }

        public static List<Cerveja> buscarCervejas(string filtro)
        {
            List<Cerveja> lista = new List<Cerveja>();

            foreach (KeyValuePair<Usuario, Cerveja> entry in listaCervejas)
            {
                if (entry.Key.Nome.ToLower().Contains(filtro.ToLower())
                    || entry.Value.Tipo.ToLower().Contains(filtro.ToLower())
                    || entry.Value.Nome.ToLower().Contains(filtro.ToLower())
                    || entry.Value.Caracteristica.ToLower().Contains(filtro.ToLower()))
                {
                    lista.Add(entry.Value);
                }
            }

            return lista;
        }

        public static void excluirCerveja(Cerveja cerveja)
        {
            foreach (KeyValuePair<Usuario, Cerveja> entry in listaCervejas)
            {
                if (entry.Value.Equals(cerveja))
                {
                    listaCervejas.Remove(entry);
                    break;
                }
            }
        }
    }
}
